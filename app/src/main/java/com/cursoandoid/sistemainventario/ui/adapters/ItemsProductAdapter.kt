package com.cursoandoid.sistemainventario.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cursoandoid.sistemainventario.data.local.entity.ProductsEntity
import com.cursoandoid.sistemainventario.databinding.ItemProductBinding

class ItemsProductAdapter (val listener: ProductClickedListener):
    ListAdapter<ProductsEntity, ItemsProductAdapter.ViewHolder>(ProductsDiffCallBack) {

    companion object ProductsDiffCallBack : DiffUtil.ItemCallback<ProductsEntity>() {
        override fun areItemsTheSame(oldItem: ProductsEntity, newItem: ProductsEntity) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: ProductsEntity, newItem: ProductsEntity) =
            oldItem == newItem
    }

    class ViewHolder (val binding: ItemProductBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(product: ProductsEntity, clickedListener: ProductClickedListener){
            //val productOne : ProductsEntity = ProductsEntity(0,"Crema","Lacteos","19","20")
            //binding.products = productOne
            binding.products = product
            binding.cardClickListener = clickedListener
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemProductBinding.inflate(
            LayoutInflater.from(parent.context), parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem,listener)
    }
}

class ProductClickedListener(val listener: (products: ProductsEntity) -> Unit) {
    fun onClick(products: ProductsEntity) = listener(products)
}