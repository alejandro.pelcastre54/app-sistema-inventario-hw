package com.cursoandoid.sistemainventario.ui.viewmodel.login

import android.app.Application
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.cursoandoid.sistemainventario.model.interfaces.LoginListener
import com.cursoandoid.sistemainventario.ui.utils.Constants

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    // Creamos variable de tipo LoginListener creado en Interface
    var loginListener: LoginListener ?= null

    // Suricatos para los EditText
    val user = ObservableField<String>("")
    val password = ObservableField<String>("")

    // Al presionar el boton btn_login nos ejecuta este metodo por "vmLogin::validateUser"
    fun validateUser(view: View){

        // Válida todas las combinaciones posibles
        if(!user.get().isNullOrEmpty() && !password.get().isNullOrEmpty()){
            if (user.get()==Constants.USER && password.get()==Constants.PASS){
                loginListener?.onSuccessLoginListener(user.get())
            }else{
                loginListener?.onErrorLoginLoginListener(Constants.ERROR_INVALID_CREDENTIALS)
            }
        }else{
            if (user.get().isNullOrEmpty() && !password.get().isNullOrEmpty()) {
                loginListener?.onErrorLoginLoginListener(Constants.ERROR_EMPTY_EMAIL)
            } else if (!user.get().isNullOrEmpty() && password.get().isNullOrEmpty()) {
                loginListener?.onErrorLoginLoginListener(Constants.ERROR_EMPTY_PASSWORD)
            } else {
                loginListener?.onErrorLoginLoginListener(Constants.ERROR_EMPTY_FIELDS)
            }
        }
    }

    // El text View actua como boton por si el usuario olvido sus credenciales
    fun forgotInformation(view: View){
        loginListener?.onQuestionLoginListener()
    }
}