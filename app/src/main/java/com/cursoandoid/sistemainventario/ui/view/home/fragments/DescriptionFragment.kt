package com.cursoandoid.sistemainventario.ui.view.home.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cursoandoid.sistemainventario.R
import com.cursoandoid.sistemainventario.databinding.FragmentDescriptionBinding

class DescriptionFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentDescriptionBinding.inflate(inflater)
        val args = DescriptionFragmentArgs.fromBundle(requireArguments()).informationProduct
        binding.apply {
            txtTitleProduct.setText(args.title)
            txtCategoryProduct.setText(args.category)
            txtStockProduct.setText(args.stock)
        }


        return binding.root
    }

}
