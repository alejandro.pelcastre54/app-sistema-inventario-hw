package com.cursoandoid.sistemainventario.ui.view.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.cursoandoid.sistemainventario.R
import com.cursoandoid.sistemainventario.databinding.ActivityLoginBinding
import com.cursoandoid.sistemainventario.model.interfaces.LoginListener
import com.cursoandoid.sistemainventario.ui.utils.Constants
import com.cursoandoid.sistemainventario.ui.view.home.activities.HomeActivity
import com.cursoandoid.sistemainventario.ui.viewUtils.toast
import com.cursoandoid.sistemainventario.ui.viewmodel.login.LoginViewModel


class LoginActivity : AppCompatActivity(),LoginListener {


    private lateinit var binding: ActivityLoginBinding   // Conexión del View con el Activity
    private lateinit var viewModel: LoginViewModel       // Conexión del View Model con el Activity
    private var constantsError: Constants = Constants    // Traemos las constantes generadas


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Databinding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        binding.vmLogin = viewModel
        viewModel.loginListener = this
    }

    override fun onSuccessLoginListener(user: String?) {
        val intent = Intent(this, HomeActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onErrorLoginLoginListener(errorType: Int) {
        when (errorType) {
            constantsError.ERROR_EMPTY_EMAIL -> toast(getString(R.string.msg_empty_email))
            constantsError.ERROR_EMPTY_PASSWORD -> toast(getString(R.string.msg_empty_password))
            constantsError.ERROR_EMPTY_FIELDS -> toast(getString(R.string.msg_empty_fields))
            constantsError.ERROR_INVALID_CREDENTIALS -> toast(getString(R.string.msg_invalid_credentials))
        }
    }

    override fun onQuestionLoginListener() {
        toast("Usuario: ${Constants.USER} \r\nConstraseña: ${Constants.PASS}")
    }
}
