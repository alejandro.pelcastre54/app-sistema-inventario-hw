package com.cursoandoid.sistemainventario.ui.viewmodel.home

import android.app.Application
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.cursoandoid.sistemainventario.data.local.db.ProductRoomDatabase
import com.cursoandoid.sistemainventario.data.local.entity.ProductsEntity
import com.cursoandoid.sistemainventario.data.local.repository.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.math.log

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    // Database
    private val productDao = ProductRoomDatabase.getDatabase(application).productDao()
    private val repository: ProductRepository
    val allProducts: LiveData<List<ProductsEntity>>


    init {
        repository = ProductRepository(productDao)
        allProducts = repository.allProductsRepository()
    }

    fun insert(product:ProductsEntity){
        viewModelScope.launch (Dispatchers.IO){
            repository.insertProductsRepository(product)
        }
    }

}