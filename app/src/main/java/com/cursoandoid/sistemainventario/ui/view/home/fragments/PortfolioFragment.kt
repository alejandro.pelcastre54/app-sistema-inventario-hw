package com.cursoandoid.sistemainventario.ui.view.home.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.cursoandoid.sistemainventario.databinding.FragmentPortfolioBinding
import com.cursoandoid.sistemainventario.ui.adapters.ItemsProductAdapter
import com.cursoandoid.sistemainventario.ui.viewmodel.home.HomeViewModel
import com.cursoandoid.sistemainventario.data.local.entity.ProductsEntity
import com.cursoandoid.sistemainventario.data.local.preferences.HomePreference
import com.cursoandoid.sistemainventario.ui.adapters.ProductClickedListener
import com.cursoandoid.sistemainventario.ui.utils.Constants


class PortfolioFragment : Fragment() {

    val productOne: ProductsEntity = ProductsEntity(0, "Crema 1/4", "Lacteos", "16.0", "20")
    val productTwo: ProductsEntity = ProductsEntity(0, "Leche 1L", "Lacteos", "22.0", "80")
    val productThree: ProductsEntity = ProductsEntity(0, "Té Manzanilla", "Bebida", "17.50", "100")
    val productFour: ProductsEntity = ProductsEntity(0, "Café Oaxaqueño", "Bebida", "123.99", "10")
    val productFive: ProductsEntity = ProductsEntity(0, "Lechuga Corazón", "Vegetales", "32.30", "30")
    val productSix: ProductsEntity = ProductsEntity(0, "Cebolla Morada", "Vegetales", "12.0", "60")

    //Preferences
    private lateinit var preferences: HomePreference

    // ViewModel
    private val viewModel: HomeViewModel by viewModels()
    private lateinit var adapter: ItemsProductAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentPortfolioBinding.inflate(inflater)

        binding.lifecycleOwner = this
        binding.vmPortfolio = viewModel

        preferences = HomePreference(requireContext())

        if(!preferences.getBolean(Constants.START)){
            viewModel.insert(productOne)
            viewModel.insert(productTwo)
            viewModel.insert(productThree)
            viewModel.insert(productFour)
            viewModel.insert(productFive)
            viewModel.insert(productSix)
            preferences.putBolean(Constants.START,true)
        }


        // Vinculamos el adapter del recycler view con este fragment
        adapter = ItemsProductAdapter(ProductClickedListener {
            findNavController().navigate(
                PortfolioFragmentDirections
                    .actionPortfolioFragmentToDescriptionFragment(it)
            )
        })

        viewModel.allProducts.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        binding.apply {

            binding.rvProducts.adapter = adapter
        }
        return binding.root
    }
}