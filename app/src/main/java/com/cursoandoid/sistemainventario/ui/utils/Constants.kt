package com.cursoandoid.sistemainventario.ui.utils

object Constants {

    const val ERROR_EMPTY_FIELDS = 1
    const val ERROR_INVALID_CREDENTIALS = 2
    const val ERROR_EMPTY_EMAIL = 3
    const val ERROR_EMPTY_PASSWORD = 4

    const val USER ="Alejandro"
    const val PASS ="Curso123"

    const val START = "start_application"
}