package com.cursoandoid.sistemainventario.data.local.preferences

import android.content.Context

class HomePreference(context:Context) {
    private val sharedPreferences = context.getSharedPreferences("FlagPreference",Context.MODE_PRIVATE)

    fun putBolean (key: String, flag: Boolean)=
        sharedPreferences.edit().putBoolean(key,flag).apply()

    fun getBolean(key: String): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }
}