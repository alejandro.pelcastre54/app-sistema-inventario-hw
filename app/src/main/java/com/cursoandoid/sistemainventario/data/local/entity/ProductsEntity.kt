package com.cursoandoid.sistemainventario.data.local.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.InvalidationTracker
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

// Creamos al Entity con la siguiente estructura
@Parcelize
@Entity(tableName = "product_table")
data class ProductsEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var title: String,
    var category: String,
    var price: String,
    var stock: String
) : Parcelable // Nos sirve para enviar info de un fragment a otro