package com.cursoandoid.sistemainventario.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.cursoandoid.sistemainventario.data.local.entity.ProductsEntity

// Los siguiente metodos en Dao nos ayudan a consultar o construir la base de datos mediante queries
@Dao
interface ProductDao {

    @Query("SELECT * from product_table ORDER BY category ASC")
    fun getAllProductsDao(): LiveData<List<ProductsEntity>>

    @Insert
    fun insertProductDao(productsEntity: ProductsEntity)
}