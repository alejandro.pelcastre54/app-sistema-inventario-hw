package com.cursoandoid.sistemainventario.data.local.repository

import androidx.lifecycle.LiveData
import com.cursoandoid.sistemainventario.data.local.dao.ProductDao
import com.cursoandoid.sistemainventario.data.local.entity.ProductsEntity

// Clase que nos permite comunicarnos con la base de datos
class ProductRepository(val productDao: ProductDao) {

    fun allProductsRepository(): LiveData<List<ProductsEntity>> = productDao.getAllProductsDao()

    suspend fun insertProductsRepository(productsRepository: ProductsEntity) =
        productDao.insertProductDao(productsRepository)
}