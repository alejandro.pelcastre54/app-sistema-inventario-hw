package com.cursoandoid.sistemainventario.model.interfaces

// Métodos sin funcionalidad  LL->Login Listener
interface LoginListener {
    fun onSuccessLoginListener(user:String?)
    fun onErrorLoginLoginListener(errorType:Int)
    fun onQuestionLoginListener()

}